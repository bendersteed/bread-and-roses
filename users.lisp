(in-package :bread-and-roses.users)

(defvar *current-user* nil)

(defclass user ()
  ((email :accessor email
	  :initarg :email)
   (password :accessor password
	     :initarg :password)))

(defvar *users* '())

(defun hash-password (password) ; add salt for rainbow attacks
  "Return the hex string of the sha256 sum of password."
  (ironclad:byte-array-to-hex-string
   (ironclad:digest-sequence
    :sha256
    (ironclad:ascii-string-to-byte-array password))))

(defun add-user (email password)
  "Add a user to *users*."
  (let ((hash (hash-password password)))
    (push (make-instance 'user
			 :email email
			 :password hash)
	  *users*)))

(defun user-from-email (email)
  "Return the user provided his email"
  (find email *users* :test #'string-equal :key #'email))

(defun authenticate (email password)
  "Return the user corresponding to the credentials or nil."
  (let ((hash (hash-password password))
	(user (user-from-email email)))
    (when (and user
	       (string-equal hash (password user)))
      user)))

(defun login (user uri)
  "Login the user and redirect to uri."
  (setf *current-user* user)
  (when hunchentoot:*session*
    (hunchentoot:remove-session hunchentoot:*session*))
  (hunchentoot:start-session)
  (setf (hunchentoot:session-value 'login-user-id) (email user))
  (hunchentoot:redirect uri))

(defun logout (uri)
  "Logout, delete session and redirect to uri."
  (when hunchentoot:*session*
    (hunchentoot:remove-session hunchentoot:*session*))
  (setf *current-user* nil)
  (hunchentoot:redirect uri))

(defun select-current-user ()
  (let ((login-user-id (hunchentoot:session-value 'login-user-id)))
    (when login-user-id
      (user-from-email login-user-id))))

(defmacro with-current-user (&body body)
  `(let ((*current-user* (select-current-user)))
     ,@body))
