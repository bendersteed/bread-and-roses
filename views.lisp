(in-package bread-and-roses.views)

(defmacro standard-page ((&key title (messages nil)) &body body)
  `(with-html-output-to-string (*standard-output* nil :prologue t :indent t)
     (:html :xmlns "http://www.w3.org/1999/xhtml"
	    :xml\:lang "en" 
	    :lang "en"
	    (:head 
	     (:meta :http-equiv "Content-Type"
		    :name "viewport"
		    :content "width=device-width, initial-scale=1")
	     (:link :rel "stylesheet"
		    :href "https://unpkg.com/picnic")
	     (:link :rel "stylesheet"
		    :href "https://fonts.googleapis.com/css?family=Special+Elite&display=swap")
	     (:link :rel "stylesheet"
		    :href "/assets/bread-and-roses.css")
	     (:title ,title))
	    (:body 
	     (:nav
	      (:a :href "#" :class "brand"
		  (:img :class "logo" :src "https://placeholder.com/wp-content/uploads/2018/10/placeholder.com-logo3.jpg"))
	      (:input :id "bemenub" :type "checkbox" :class "show")
	      (:label :for "bemenub" :class "burger pseudo button" "☰")
	      (:div :class "menu"
		    (:a :href "/" :class "pseudo button" "Home")
		    (:a :href "/about" :class "pseudo button" "About")
		    (if *current-user*
			(htm
			 (:a :href "/members" :class "pseudo button" "Members Only")
			 (:a :href "/logout" :class "button"
			     (cl-who:fmt "Logout -- ~a" (email *current-user*)))))))
	     (:div :class "container"
		   (when ,messages
		     (htm (:div
			   :class "error full third-700 off-third-700"
			   (cl-who:fmt messages))))
		   ,@body)
	     (:script :src "/assets/bread-and-roses.js")))))

(defun links-for-hashtags (description uri)
  "Given a description string return the string with the appropriate
  html links. Take notice that it removes the prescending #."
  (cl-ppcre:regex-replace-all *hashtag-scanner* description
			      (format nil "<a href=\"~a?tag=\\1\"> \\& </a>" uri)))

(defmacro picture-card (&body body)
  `(htm (:div
	 (:article :class "card polaroid"
		   (:img :src (source picture) :alt (description picture))
		   (:footer
		    (:h3 (cl-who:fmt "~a" (cl-who:escape-string-minimal (title picture))))
                    (:p (cl-who:fmt "~a" (date picture)))
		    (:p (cl-who:fmt "~a" (links-for-hashtags
                                          (cl-who:escape-string-minimal (description picture)) "/")))
		    ,@body)))))

(defun home-page (&key messages tag)
  (let ((pictures (if tag
		      (pictures :pictures (hashtag-pictures tag))
		      (pictures))))
    (standard-page (:title "Bread and Roses | Home" :messages messages)
      (when tag
	(htm (:h3 (cl-who:fmt "Pictures with hashtag ~a"
			      (format nil "#~a" tag))
		  (:a :href "/" :class " button" "<- All pictures"))))
      (:div :id "polaroid-container" :class "flex one two-600 three-1000"
	    (dolist (picture pictures)
	      (picture-card
		(htm (:a :href (format nil "/picture?title=~a" (slug picture))
			 :class "button" "View more")))))
      (unless *current-user*
	(htm (:div :class "login form full third-700 off-third-700"
		   (:h4 "If you are a member of the project login.")
		   (:form :action "/login" :method "post"
			  (:input :class "stack" :name "email" :type "email" :placeholder "Your email")
			  (:input :class "stack" :name "password" :type "password" :placeholder "Your password")
			  (:input :class "stack" :value "Submit" :type "submit"))))))))

(defun members-page (&key messages)
  (standard-page (:title "Bread and Roses | Members Only" :messages messages)
    (:div :class "new-picture full third-700 off-third-700"
	  (:h2 "Upload your new picture!")
	  (:form :action "/addpicture" :method "post" :enctype "multipart/form-data"
		 (:label :class "dropimage"
			 (:input :type "file" :required "" :name "image"))
		 (:input :class "stack" :type "text" :name "title"
			 :pattern "[A-Za-z0-9- ]+"
			 :required "" :placeholder "Title")
		 (:textarea :class "stack" :name "description" :required "" :placeholder "Description" )
		 (:input :class "stack" :value "Submit" :type "submit")))))

(defun about-page (&key messages)
  (standard-page (:title "Bread and Roses | About")
    (:div :class "full three-fifth-700 off-fifth-700 "
	  (:h2 "Who we are")
	  (:p "It would be nice to have a section for explaining the
     purpose and the founding process of this website. A nice
     empowering paragraph would be great.")
	  (:hr)
	  (:h2 "About this website")
	  (:a :rel "license" :href "http://creativecommons.org/licenses/by-nc/4.0/"
	      (:img :alt "Creative Common License" :style "border-width:0; margin: auto;"
		    :src "https://i.creativecommons.org/l/by-nc/4.0/88x31.png"))
	  (:br)
	  (:p "All content in this website is licensed under a "
	      (:a :rel "license" :href "http://creativecommons.org/licenses/by-nc/4.0/"
		  "Creative Commons Attribution-NonCommercial 4.0 International License"))
	  (:p "You can study and use this website by getting the code at "
	      (:a :href "https://gitlab.com/bendersteed/bread-and-roses" "gitlab."))
	  (:p "It wouldn't be possible to create this website without the wonderful work of:"
	      (:ul
	       (:li (:a :href "https://edicl.github.io/hunchentoot/" "Hunchentoot")
		    " -- web server, routing, session management and more.")
	       (:li (:a :href "https://edicl.github.io/cl-who/" "CL-WHO")
		    " --  makes writing HTML fun.")
	       (:li (:a :href "https://edicl.github.io/cl-fad/" "CL-FAD")
		    " -- for dealing with filenames and filepaths in Common Lisp.")
	       (:li (:a :href "http://method-combination.net/lisp/ironclad/" "Ironclad")
		    " -- for various cryptographic needs.")
	       (:li (:a :href "http://sbcl.org/" "SBCL")
		    " -- for being a wonderful Common Lisp implementation.")
	       (:li (:a :href "http://heroku.com/" "Heroku")
		    " -- for providing a platform on which we could easily deploy.")
	       (:li (:a :href "https://gitlab.com/duncan-bayne/heroku-buildpack-common-lisp" "Duncan Bayne")
		    " -- for doing the work and writing the build pack to deploy Common Lisp applications to heroku.")
	       (:li (:a :href "https://picnicss.com/" "Picnic CSS")
		    " -- a minimal and very powerfull CSS framework.")))
	  (:img :src "/assets/img/static/lisp.png"))))

(defun picture-page (picture &key messages)
  (standard-page (:title (cl-who:fmt "Bread and Roses | ~a" (title picture)))
    (:div :id "single-polaroid" :class "flex one"
	  (picture-card))))

(defun not-found-page (&key messages)
  (standard-page (:title "Bread and Roses | Not found")
    (:div :id "single-polaroid" :class "flex one"
	  (:div
	   (:article :class "card polaroid"
		     (:h1 :style "text-align: center;" "404 - Not Found")
		     (:img :src "/assets/img/static/404.gif" :alt "There is nothing here")
		     (:footer
		      (:h1 "There is nothing here. Please go back!")))))))
