(defpackage :bread-and-roses.hashtags
  (:use #:cl)
  (:export :hashtag-pictures
           :*hashtag-scanner*
           :*hashtags*))

(defpackage :bread-and-roses.pictures
  (:use #:cl)
  (:import-from #:bread-and-roses.hashtags
                #:*hashtags*
                #:*hashtag-scanner*)
  (:export :picture
	   :title
	   :source
           :description
           :date
	   :pictures
	   :add-picture
	   :picture-from-title
	   :picture-exists
           :*pictures*
           :slug
           :create-slug))

(defpackage :bread-and-roses.users
  (:use #:cl)
  (:export :authenticate
	   :user-from-email
	   :email
           :*current-user*
           :add-user
           :login
           :logout
           :select-current-user
           :with-current-user))

(defpackage :bread-and-roses.image
  (:use #:cl)
  (:import-from #:cl-fad #:copy-file)
  (:export :validate-image-p
	   :store-image))

(defpackage :bread-and-roses.views
  (:use #:cl)
  (:import-from #:cl-who #:with-html-output-to-string
		#:htm)
  (:import-from #:bread-and-roses.hashtags
                #:hashtag-pictures
                #:*hashtag-scanner*)
  (:import-from #:bread-and-roses.users
                #:*current-user*
                #:email)
  (:import-from #:bread-and-roses.pictures
	        #:title
                #:source
                #:description
                #:date
                #:pictures
                #:slug)
  (:export :home-page
	   :members-page
	   :about-page
	   :picture-page
	   :not-found-page))

(defpackage :bread-and-roses.web
  (:use #:cl #:bread-and-roses.views
        #:bread-and-roses.image)
  (:import-from #:bread-and-roses.users
                #:*current-user*
                #:with-current-user
                #:authenticate
                #:login
                #:logout)
  (:import-from #:bread-and-roses.pictures
                #:picture-from-title
                #:pictures
                #:add-picture
                #:create-slug)
  (:export :start-dev-server
	   :*current-user*))
