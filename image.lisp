(in-package bread-and-roses.image)

(defparameter *image-directory* (make-pathname :directory `(:relative "assets" "img")))

(ensure-directories-exist *image-directory*)

(defparameter *accepted-mimetypes* '("image/x-png" "image/png" "image/jpeg" "image/pjpeg"))
(defparameter *accepted-image-size* 3000000)

(defun file-size (filename)
  "Calculate the size of a file."
  (with-open-file (stream filename :direction :input :if-does-not-exist nil)
    (file-length stream)))

(defun correct-image-type-p (hunchentoot-image-tuple)
  "Checks if the uploaded image is of correct type, by checking its
mimetype."
  (member (third hunchentoot-image-tuple) *accepted-mimetypes* :test #'equal))

(defun correct-image-size-p (hunchentoot-image-tuple)
  "Checks if the uploaded image has acceptable size."
  (< (file-size (first hunchentoot-image-tuple)) *accepted-image-size*))

(defun validate-image-p (hunchentoot-image-tuple)
  "Validate whether the uploaded image is appropriate for saving to
the server."
  (and (correct-image-size-p hunchentoot-image-tuple)
       (correct-image-type-p hunchentoot-image-tuple)))

(defun image-type (mimetype)
  "Given the mimetype assert the image's type."
  (cond
    ((member mimetype '("image/x-png" "image/png") :test #'equal) "png")
    ((member mimetype '("image/jpeg" "image/pjpeg") :test #'equal) "jpg")))

(defvar *stream-buffer-size* 8192)

(defun store-image (hunchentoot-image-tuple slug)
  "Given a hunchentoot-image-tuple for a validated image and slug,
  save it to the appropriate folder."
  (let* ((filepath (first hunchentoot-image-tuple))
	 (mimetype (third hunchentoot-image-tuple))
	 (image-new-name (make-pathname :name slug :type (image-type mimetype))))
    (copy-file filepath (merge-pathnames image-new-name
					 (merge-pathnames *image-directory*)))
    (namestring (merge-pathnames image-new-name *image-directory*))))
