(in-package :bread-and-roses.hashtags)

(defvar *hashtags* '())

(defvar *hashtag-scanner*
  (cl-ppcre:create-scanner "(?i)#([A-Za-z0-9]*)"))

(defun hashtag-pictures (hashtag)
  "Return a list of the pictures that match the given hashtag."
  (cdr (assoc (format nil "#~a" hashtag) *hashtags* :test #'equal)))
