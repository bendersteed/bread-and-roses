(in-package :bread-and-roses.pictures)

(defun format-date ()
  (multiple-value-bind (seconds minutes hours day month year)
      (get-decoded-time)
      (format nil "~2,'0d/~d/~d" day month year)))

(defclass picture ()
  ((title :accessor title
	  :initarg :title)
   (source :accessor source
	   :initarg :source)
   (description :accessor description
		:initarg :description)
   (date :accessor date
         :initform (format-date))
   (slug :accessor slug)))

(defmethod initialize-instance :after ((picture picture) &rest initargs)
  (with-slots (title slug) picture
    (setf slug (create-slug title))))

(defvar *pictures* '())

(defun picture-from-title (slug)
  "Grab a picture object from *pictures* by its slugified title."
  (find slug *pictures* :test #'string-equal
			:key #'slug))

(defun picture-exists-p (title)
  "Check if picture with the same title exists in the database."
  (picture-from-title title))

(defun pictures (&key (pictures *pictures*))
  "Return a sorted list of all the pictures."
  pictures) ; this probably needs to be sorted by date

(defun add-hashtag (new-picture)
  (let* ((description (description new-picture))
	 (hashtags (cl-ppcre:all-matches-as-strings
		    *hashtag-scanner* description)))
    (loop for hashtag in hashtags
	  if (assoc hashtag *hashtags* :test #'equal)
	    do  (push new-picture (cdr (assoc hashtag *hashtags* :test #'equal)))
	  else
	    do (push (cons hashtag (list new-picture)) *hashtags*))))

(defun add-picture (title source description)
  "Add a picture to *pictures* and create it's appropriate hashtags."
  (let ((picture (make-instance 'picture
				:title title
				:source source
				:description description)))
    (add-hashtag picture)
    (push picture *pictures*)))

(defun create-slug (title)
  "Grab a title and return a string proper for a filename and url."
  (string-downcase
   (cl-ppcre:regex-replace-all "%20"
			       (hunchentoot:url-encode title) "-")))
