(defpackage #:bread-and-roses-asd
  (:use :cl :asdf))

(in-package :bread-and-roses-asd)

(defsystem bread-and-roses
  :name "Bread and Roses"
  :version "0.0.1"
  :maintainer "Dimakakos Dimos aka bendersteed"
  :author "Dimakakos Dimos"
  :licence "GPL v3.0"
  :long-description "An idea conceived at the Bread & Roses youth
exchange, organised by Lunaria. It is a web app where the members can
vote on proposed images, that if accepted create a collage of imagery
with the purpose of spreading awareness on gender issues."
  :description "Web application for the collection of gender related
  images."
  :serial t
  :depends-on (:hunchentoot
	       :cl-who
	       :cl-fad
	       :ironclad
	       :cl-ppcre)
  :components ((:file "packages")
               (:file "hashtags")
               (:file "pictures")
	       (:file "users")
	       (:file "image")
	       (:file "views")
	       (:file "web")
               (:file "heroku")))
