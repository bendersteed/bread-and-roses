(in-package bread-and-roses.web)

(defmacro define-route ((&key uri title args (authentication nil)) &body body)
  `(hunchentoot:define-easy-handler (,title :uri ,uri) ,args
     (setf (hunchentoot:content-type*) "text/html")
     (with-current-user
	 ,@(when authentication
	     `((unless *current-user*
		 (hunchentoot:redirect "/404"))))
       ,@body)))

(define-route (:uri "/" :title home :args (tag))
    (home-page :tag tag))

(define-route (:uri "/members" :title members :args () :authentication t)
    (members-page))

(define-route (:uri "/about" :title about :args ())
    (about-page))

(define-route (:uri "/addpicture" :title addpicture :args (image title description) :authentication t)
    (if (validate-image-p image)
	(progn (add-picture title (store-image image (create-slug title)) description)
	       (hunchentoot:redirect "/members"))
	(members-page :messages "You should upload an image that is JPG or PNG and has a size of at most 3MBs. 
The other fields are required as well.")))

(define-route (:uri "/picture" :title picture :args (title))
    (let ((picture (picture-from-title title)))
      (if (member picture (pictures))
	  (picture-page picture)
          (hunchentoot:redirect "/"))))

(define-route (:uri "/login" :title login-user :args (email password))
    (let ((user (authenticate email password)))
      (if user
	  (login user "/")
	  (home-page :messages "Invalid credentials."))))

(define-route (:uri "/logout" :title logout-user :args ())
    (logout "/"))

(defmethod hunchentoot:acceptor-status-message (acceptor (http-status-code (eql 404)) &key)
  (not-found-page))

;; for development
(defvar *app-dev* (make-instance 'hunchentoot:easy-acceptor :port 4242))

(defun start-dev-server ()
  (setf hunchentoot:*dispatch-table*     
	`(hunchentoot:dispatch-easy-handlers
	  ,(hunchentoot:create-folder-dispatcher-and-handler
	    "/assets/" "~/src/bread-roses/assets/")))
  (hunchentoot:start *app-dev*))
